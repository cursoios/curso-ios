//
//  CIOSinglePickerViewController.h
//  Demo_9
//
//  Created by Mario Caballero Ramírez on 4/10/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIOSinglePickerViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) NSArray *countries;
@property (strong, nonatomic) NSArray *languages;

@end
