//
//  CIODatePickerViewController.m
//  Demo_9
//
//  Created by Mario Caballero Ramírez on 4/10/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIODatePickerViewController.h"

@interface CIODatePickerViewController ()

@end

@implementation CIODatePickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)formatDate : (NSDate*) aDate {
  NSDateFormatter* dateFormat = [NSDateFormatter new];
  [dateFormat setDateFormat:@"dd-MM-yyyy 'a las' HH:mm:ss"];
  return [dateFormat stringFromDate:aDate];
}

- (IBAction)atSelectDate:(id)sender {
  NSDate* pickerDate = self.picker.date;
  
  NSString *messageText = [[NSString alloc]
                           initWithFormat:@"¡Has seleccionado la fecha %@!", [self formatDate:pickerDate]];
  
  UIAlertView* message = [[UIAlertView alloc] initWithTitle:@""
                                              message: messageText
                                              delegate:nil
                                              cancelButtonTitle:@"Yes"
                                              otherButtonTitles:nil];
  [message show];
}

@end
