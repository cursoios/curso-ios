//
//  CIODependentPickerViewController.m
//  Demo_9
//
//  Created by Mario Caballero Ramírez on 4/10/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIODependentPickerViewController.h"

@interface CIODependentPickerViewController ()

@end

@implementation CIODependentPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.countries = @[@"España", @"Francia", @"Alemania"];
  
    self.cities = @{@"España" : @[@"Las Palmas", @"Madrid", @"Barcelona"],
                    @"Francia" : @[@"Toulouse"],
                    @"Alemania" : @[@"Frankfurt", @"Zurich"]
    };
  
    self.picker.dataSource = self;
    self.picker.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
  return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
  
  if (component == 0)
    return self.countries.count;

  NSString *selectedCountry = [self.countries objectAtIndex:[self.picker selectedRowInComponent:0]];
  
  return [[self.cities objectForKey:selectedCountry] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
  
  if (component == 0)
    return [self.countries objectAtIndex:row];
  
  NSString *selectedCountry = [self.countries objectAtIndex:[self.picker selectedRowInComponent:0]];
  
  return [[self.cities objectForKey:selectedCountry] objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
  
  NSInteger selectedIndex = [pickerView selectedRowInComponent:0];
  
  if (component == 0 && !(selectedIndex < 0)) {
    [self.picker reloadComponent:1];
    [self.picker selectRow:0 inComponent:1 animated:YES];
  }
  else {
    int countryIndex = [self.picker selectedRowInComponent:0];
    int cityIndex = [self.picker selectedRowInComponent:1];
    NSString *country = [self.countries objectAtIndex:countryIndex];
    NSString *city = [[self.cities objectForKey:country] objectAtIndex:cityIndex];
    
    NSString *messageText = [[NSString alloc]
                             initWithFormat:@"¡Has seleccionado %@ en %@!", city, country];
    
    UIAlertView* message = [[UIAlertView alloc] initWithTitle:@""
                                                      message: messageText
                                                     delegate:nil
                                            cancelButtonTitle:@"Yes"
                                            otherButtonTitles:nil];
    [message show];
  }
  
}

@end
