//
//  CIOSinglePickerViewController.m
//  Demo_9
//
//  Created by Mario Caballero Ramírez on 4/10/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIOSinglePickerViewController.h"

@interface CIOSinglePickerViewController ()

@end

@implementation CIOSinglePickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.countries = @[@"España", @"Francia", @"Alemania"];
    self.languages = @[@"Español", @"Francés", @"Inglés"];

    self.picker.dataSource = self;
    self.picker.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
  return 1;
  //return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
  
  if (component == 0)
    return self.countries.count;
  
  return [self.languages count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
  
  if (component == 0)
    return [self.countries objectAtIndex:row];
  
  return [self.languages objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
  
  int index = [self.picker selectedRowInComponent:0];
  
  NSString *messageText = [[NSString alloc]
                           initWithFormat:@"¡Has seleccionado %@!", [self.countries objectAtIndex:index]];
  
  UIAlertView* message = [[UIAlertView alloc] initWithTitle:@""
                                              message: messageText
                                              delegate:nil
                                              cancelButtonTitle:@"Yes"
                                              otherButtonTitles:nil];
  [message show];  
}

@end
