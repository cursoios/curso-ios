//
//  CIODatePickerViewController.h
//  Demo_9
//
//  Created by Mario Caballero Ramírez on 4/10/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIODatePickerViewController : UIViewController
- (IBAction)atSelectDate:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *picker;

@end
