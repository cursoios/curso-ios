//
//  CIOViewController.h
//  Demo_12
//
//  Created by Mario Caballero Ramírez on 4/16/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIOViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
- (IBAction)atSave:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *FieldName;
@property (weak, nonatomic) IBOutlet UITextField *FieldPhone;
- (IBAction)atAdd:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *ViewContacts;

@end
