//
//  Contact.h
//  Demo_12
//
//  Created by Mario Caballero Ramírez on 4/16/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject<NSCoding>

@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *Phone;

- (id)initWithCoder:(NSCoder *)aDecoder;

- (void)encodeWithCoder:(NSCoder *)aCoder;

@end
