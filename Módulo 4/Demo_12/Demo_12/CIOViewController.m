//
//  CIOViewController.m
//  Demo_12
//
//  Created by Mario Caballero Ramírez on 4/16/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIOViewController.h"
#import "Contact.h"

@interface CIOViewController ()

@property (strong, nonatomic) NSMutableArray *Contacts;

@end

@implementation CIOViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadContacts];
  
    self.ViewContacts.delegate = self;
    self.ViewContacts.dataSource = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {  
  UITouch *touch = [[event allTouches] anyObject];
  if ([self.FieldName isFirstResponder] && [touch view] != self.FieldName) {
    [self.FieldName resignFirstResponder];
  }
  [super touchesBegan:touches withEvent:event];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [self.Contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Cell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
  Contact *contact = [self.Contacts objectAtIndex:indexPath.row];
  cell.textLabel.text = contact.Name;
  cell.detailTextLabel.text = contact.Phone;
  
  return cell;
}

- (NSString*) getDocumentsDirectory {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  return [paths objectAtIndex:0];
};

- (void) loadContacts {
  //[self loadContactsFromSerialization];
  [self loadContactsFromJson];
}

- (void) loadContactsFromSerialization {
  NSString *contactsPath = [[self getDocumentsDirectory] stringByAppendingPathComponent:@"contacts.nscoder"];
  NSFileManager* fileManager = [NSFileManager defaultManager];
  BOOL fileExists = [fileManager fileExistsAtPath:contactsPath];
  
  self.Contacts = [NSMutableArray new];
  if (fileExists)
    self.Contacts = [NSKeyedUnarchiver unarchiveObjectWithFile:contactsPath];
}

- (void) loadContactsFromJson {
  NSError *error;
  NSString *contactsPath = [[self getDocumentsDirectory] stringByAppendingPathComponent:@"contacts.json"];
  NSFileManager* fileManager = [NSFileManager defaultManager];
  BOOL fileExists = [fileManager fileExistsAtPath:contactsPath];
  
  self.Contacts = [NSMutableArray new];
  if (fileExists) {
    NSArray *contacts;
    
    NSInputStream *inputStream = [[NSInputStream alloc] initWithFileAtPath:contactsPath];
    [inputStream open];
    contacts = [NSJSONSerialization JSONObjectWithStream:inputStream options:NSJSONWritingPrettyPrinted error:&error];
    [inputStream close];
    
    for (NSString *contactName in contacts) {
      Contact *contact = [Contact new];
      contact.Name = contactName;
      [self.Contacts addObject: contact];
    }
  }
}

- (void) saveContacts {
  [self saveContactsSerialized];
  [self saveContactsInJson];
}

- (void) saveContactsSerialized {
  NSString *documentsDir = [self getDocumentsDirectory];
  
  NSString *contactsPath = [documentsDir stringByAppendingPathComponent:@"contacts.nscoder"];
  [NSKeyedArchiver archiveRootObject:self.Contacts toFile:contactsPath];
}

- (void) saveContactsInJson {
  NSError *error;
  NSString *documentsDir = [self getDocumentsDirectory];
  NSString *jsonContactsPath = [documentsDir stringByAppendingPathComponent:@"contacts.json"];
  NSMutableArray *contacts = [NSMutableArray new];
  
  for (Contact *contact in self.Contacts)
    [contacts addObject: contact.Name];

  NSOutputStream *outputStream = [NSOutputStream outputStreamToFileAtPath:jsonContactsPath append:true];
  [outputStream open];
  [NSJSONSerialization writeJSONObject:contacts toStream:outputStream options:NSJSONWritingPrettyPrinted error:&error];
  [outputStream close];
}

- (IBAction)atSave:(id)sender {
  [self saveContacts];
  
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Guardar" message:@"Se han guardado los contactos" delegate:self cancelButtonTitle:@"Cerrar" otherButtonTitles:nil];
  
  [alertView show];
}

- (IBAction)atAdd:(id)sender {
  [self.FieldName resignFirstResponder];
  [self.FieldPhone resignFirstResponder];
  
  Contact *contact = [Contact new];
  contact.Name = self.FieldName.text;
  contact.Phone = self.FieldPhone.text;
  
  [self.Contacts addObject:contact];
  [self.ViewContacts reloadData];
}

@end
