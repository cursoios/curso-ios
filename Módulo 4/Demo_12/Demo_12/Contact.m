//
//  Contact.m
//  Demo_12
//
//  Created by Mario Caballero Ramírez on 4/16/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (id)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super init]) {
    self.Name = [aDecoder decodeObjectForKey:@"name"];
    self.Phone = [aDecoder decodeObjectForKey:@"phone"];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
  [aCoder encodeObject:self.Name forKey:@"name"];
  [aCoder encodeObject:self.Phone forKey:@"phone"];
}

@end
