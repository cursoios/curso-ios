//
//  main.m
//  Demo_12
//
//  Created by Mario Caballero Ramírez on 4/16/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CIOAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([CIOAppDelegate class]));
  }
}
