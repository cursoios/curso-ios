//
//  CIOSettingsViewController.h
//  Demo_13
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIOSettingsViewController : UITableViewController
- (IBAction)atSave:(id)sender;

@end
