//
//  CIOPreferencesViewController.m
//  Demo_13
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIOPreferencesViewController.h"

@interface CIOPreferencesViewController ()

@property (strong, nonatomic) NSArray *Contacts;

@end

@implementation CIOPreferencesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadContacts];
}

- (void) loadContacts {
  NSString *error;
  NSPropertyListFormat format;
  NSString *contactsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"contacts.plist"];
  //NSString *contactsPath = [[NSBundle mainBundle] pathForResource:@"contacts" ofType:@"plist"];
  NSFileManager *fileManager = [NSFileManager defaultManager];
  
  if (! [fileManager fileExistsAtPath:contactsPath])
    self.Contacts = @[ @"Jose Juan", @"Rayco", @"Mario" ];
  else {
    NSData *contactsXML = [fileManager contentsAtPath:contactsPath];
    self.Contacts = (NSArray *)[NSPropertyListSerialization
                                propertyListFromData:contactsXML
                                mutabilityOption: NSPropertyListImmutable
                                format:&format errorDescription:&error];
  }
}

- (void) saveContacts {
  NSError *error;
  NSString *errorString;
  NSString *contactsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"contacts.plist"];
  
  id propertyList = [NSPropertyListSerialization dataFromPropertyList:self.Contacts format:NSPropertyListXMLFormat_v1_0 errorDescription:&errorString];
  
  [propertyList writeToFile:contactsPath options:NSDataWritingAtomic error:&error];
  
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Guardar" message:@"Se ha guardado la lista de propiedades" delegate:self cancelButtonTitle:@"Cerrar" otherButtonTitles:nil, nil];
  
  [alertView show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.Contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    NSString *contact = [self.Contacts objectAtIndex:indexPath.row];
    cell.textLabel.text = contact;
  
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (IBAction)atSave:(id)sender {
  [self saveContacts];
  [self.tableView reloadData];
}
@end
