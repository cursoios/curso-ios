//
//  CIOSettingsViewController.m
//  Demo_13
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIOSettingsViewController.h"

@interface CIOSettingsViewController ()

@property (strong, nonatomic) NSDictionary *Preferences;

@end

@implementation CIOSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self loadPreferences];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
  
    [self loadPreferences];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadPreferences {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  NSString *fullName = [userDefaults valueForKey:@"fullname_preference"];
  NSString *age = [userDefaults valueForKey:@"age_preference"];
  
  if (fullName == nil) fullName = @"";
  if (age == nil) age = @"";
  
  self.Preferences = @{@"Nombre" : fullName, @"Edad" : age};
}

- (void) savePreferences {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  NSString *fullName = @"David Ramírez";
  NSString *age = @"18";
  
  [userDefaults setObject:fullName forKey:@"fullname_preference"];
  [userDefaults setObject:age forKey:@"age_preference"];
  [userDefaults synchronize];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.Preferences count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
    NSArray *keys = [self.Preferences allKeys];
    NSString *preference = [keys objectAtIndex:indexPath.row];
  
    cell.textLabel.text = preference;
    cell.detailTextLabel.text = [self.Preferences valueForKey: preference];
  
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (IBAction)atSave:(id)sender {
  [self savePreferences];
  [self loadPreferences];
  [self.tableView reloadData];
}

@end
