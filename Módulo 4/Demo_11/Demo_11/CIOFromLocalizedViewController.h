//
//  CIOFromLocalizedViewController.h
//  Demo_11
//
//  Created by Mario Caballero Ramírez on 4/10/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIOFromLocalizedViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
