//
//  CIOViewController.h
//  Demo_14
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIOViewController : UITableViewController
- (IBAction)atSave:(id)sender;

@end
