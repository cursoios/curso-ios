//
//  Contact.m
//  Demo_14
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "Contact.h"


@implementation Contact

@dynamic name;
@dynamic phone;

@end
