//
//  CIOViewController.m
//  Demo_14
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIOViewController.h"
#import "CIOAppDelegate.h"
#import "Contact.h"

@interface CIOViewController ()

@property (strong, nonatomic) NSArray *Contacts;

@end

@implementation CIOViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadContacts];

    if ([self.Contacts count] == 0)
      [self createContacts];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) createContacts {
  CIOAppDelegate *applicationDelegate = [[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = [applicationDelegate managedObjectContext];
  
  Contact* contact1 = (Contact*)[NSEntityDescription
                                 insertNewObjectForEntityForName:@"Contact"
                                 inManagedObjectContext:context];
  contact1.name = @"Jose Juan";
  contact1.phone = @"928.222.111";
  
  Contact* contact2 = (Contact*)[NSEntityDescription
                                 insertNewObjectForEntityForName:@"Contact"
                                 inManagedObjectContext:context];
  contact2.name = @"Rayco";
  contact2.phone = @"928.333.111";
  
  self.Contacts = @[ contact1, contact2 ];
}

- (void) loadContacts {
  NSError *error;
  CIOAppDelegate *applicationDelegate = [[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = [applicationDelegate managedObjectContext];
  NSEntityDescription* entityDescription = [NSEntityDescription
                                            entityForName:@"Contact"
                                            inManagedObjectContext:context];
  
  NSFetchRequest* fetchRequest = [NSFetchRequest new];
  [fetchRequest setEntity:entityDescription];
  
  self.Contacts = [context executeFetchRequest:fetchRequest error:&error];
}

- (void) saveContacts {
  NSError *error;
  CIOAppDelegate *applicationDelegate = [[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = [applicationDelegate managedObjectContext];
  [context save:&error];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.Contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Contact *contact = [self.Contacts objectAtIndex:indexPath.row];
    cell.textLabel.text = contact.name;
    cell.detailTextLabel.text = contact.phone;
  
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (IBAction)atSave:(id)sender {
  [self saveContacts];
}
@end
