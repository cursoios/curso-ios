//
//  CIOAppDelegate.h
//  Demo_14
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
