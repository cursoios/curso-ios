//
//  CIOViewController.h
//  Demo_15
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIOViewController : UIViewController<NSURLConnectionDelegate>
@property (weak, nonatomic) IBOutlet UITextView *TextView;

@end
