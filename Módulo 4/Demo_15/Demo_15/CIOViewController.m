//
//  CIOViewController.m
//  Demo_15
//
//  Created by Mario Caballero Ramírez on 4/17/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import "CIOViewController.h"

@interface CIOViewController ()

@property (strong, nonatomic) NSMutableData *ReceivedData;

@end

@implementation CIOViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    [self doRequest];
}

- (void) doRequest {
  NSURLRequest *request= [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.elmundo.es"]];
  
  NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
  self.TextView.text = @"Realizando la petición. Por favor, espera...";

  if (connection)
    self.ReceivedData = [NSMutableData new];
  else
    self.TextView.text = @"La petición ha fallado";
  
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
  [self.ReceivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  NSString *resultPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"result.html"];
  
  [self.ReceivedData appendData:data];
  [self.ReceivedData writeToFile:resultPath atomically:true];
  
  NSString *message = [NSString stringWithFormat:@"Ve a la ruta '%@' para ver el resultado", resultPath];
  
  self.TextView.text = message;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
