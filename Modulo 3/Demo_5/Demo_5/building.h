#import <Foundation/Foundation.h>
#import "building.h"

@interface Building : NSObject {
  NSString* name;
  NSString* architect;
  NSNumber* age;
  NSNumber* floorsCount;
  NSNumber* floorsLong;
  NSNumber* floorsWide;
  NSDate* finished;
}

@property (retain) NSString* Name;
@property (retain) NSString* Architect;
@property (retain) NSNumber* Age;
@property (retain) NSNumber* FloorsCount;
@property (retain) NSNumber* FloorsLong;
@property (retain) NSNumber* FloorsWide;
@property (retain) NSDate* Finished;

+ (Building*)buildingWithName: (NSString*)aName;
- (bool)isNewBuilding;
- (NSNumber*)getFloorSquareMeters;
- (NSString*)getBuildingSquareMeters;

@end
