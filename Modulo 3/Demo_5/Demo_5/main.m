#import <Foundation/Foundation.h>
#import "building.h"

int main(int argc, const char * argv[])
{

  @autoreleasepool {
    Building *building1 = [Building new];
    building1.Name = @"Edificio 1";
    
    Building *building2 = [Building new];
    building2.Name = @"Edificio 2";
    building2.Finished = [NSDate new];
  }
    return 0;
}

