#import "building.h"

@implementation Building

@synthesize Name;
@synthesize Architect;
@synthesize Age;
@synthesize FloorsCount;
@synthesize Finished;

+ (Building*)buildingWithName: (NSString*)aName {
  Building *building = [Building new];
  building.Name = aName;
  return building;
}

- (bool)isNewBuilding {
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *components = [calendar components: NSYearCalendarUnit fromDate:Finished];
  NSInteger year = [components year];
  return year > 2008;
}

- (NSNumber*)getFloorSquareMeters {
  int floorLongInt = [self.FloorsLong intValue];
  int floorWideInt = [self.FloorsWide intValue];
  
  return [NSNumber numberWithInt:floorLongInt*floorWideInt];
}

- (NSString*)getBuildingSquareMeters {
  int floorsCountInt = [self.FloorsCount intValue];
  int floorMeters = [[self getFloorSquareMeters] intValue];
  NSNumber *buildingMeters = [NSNumber numberWithInt:floorsCountInt*floorMeters];
  NSString *result = [NSString stringWithFormat:@"%@ m2", buildingMeters];
  
  return result;
}

@end
