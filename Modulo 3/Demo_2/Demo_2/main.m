#import <Foundation/Foundation.h>
#import "building.h"

void fillBuildingDate(Building *building) {
  NSString *str =@"3/15/2011 9:15 PM";
  NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
  [formatter setDateFormat:@"MM/dd/yyyy HH:mm a"];
  building.Finished = [formatter dateFromString:str];
}

void fillBuilding(Building *building) {
  building.Name = @"Edificio de Informática";
  fillBuildingDate(building);
}

void printBuildingDate(Building *building) {
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"yyyy/MM/dd"];
  [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
  
  NSLog(@"\nFecha de finalización: %@\n", [formatter stringFromDate:building.Finished]);
  NSLog(@"\n¿Es un edificio nuevo?: %s", [building isNewBuilding]?"SI":"NO");
}

void printBuilding(Building *building) {
  NSLog(@"\nNombre: %@\n", building.Name);
  printBuildingDate(building);
}

int main(int argc, const char * argv[])
{

  @autoreleasepool {
    Building *building = [Building new];
    fillBuilding(building);
    printBuilding(building);
  }
    return 0;
}

