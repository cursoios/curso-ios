struct Persona {
  char *nombre;
  char *apellidos;
  int edad;
  float peso;
  float altura;
};

typedef struct Persona Persona;