#include <stdio.h>
#import "person.c"

void rellenarPersona(Persona *persona)
{
  persona->nombre = "Mario";
  persona->apellidos = "Caballero Ramírez";
  persona->edad = 30;
  persona->altura = 165.5;
  persona->peso = 100;
}

void imprimirPersona(Persona *persona)
{
  printf("Nombre: %s\n", persona->nombre);
  printf("Apellidos: %s\n", persona->apellidos);
  printf("Edad: %d\n", persona->edad);
  printf("Altura: %f\n", persona->altura);
  printf("Peso: %f\n", persona->peso);
  
  if (persona->peso >= 100 && persona->altura <= 170.0)
    printf("\n\n%s %s necesita bajar de peso", persona->nombre, persona->apellidos);
  else
    printf("\n\n%s %s tiene un peso normal", persona->nombre, persona->apellidos);
}

int main(int argc, const char * argv[])
{
  @autoreleasepool {
    Persona persona;
    rellenarPersona(&persona);
    imprimirPersona(&persona);
  }
  
  return 0;
}

