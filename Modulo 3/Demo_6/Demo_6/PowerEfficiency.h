//
//  PowerEficiency.h
//  Demo_6
//
//  Created by Mario Caballero Ramírez on 4/2/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PowerEfficiency <NSObject>

- (NSString*) getLevel;

@end
