#import <Foundation/Foundation.h>
#import "ShoppingCenter.h"
#import "Institute.h"

int main(int argc, const char * argv[])
{

  @autoreleasepool {
    Building *shoppingCenter = [ShoppingCenter new];
    shoppingCenter.Name = @"Edificio 1";
    
    printf("El nivel de eficiencia del centro comercial %s es %s\n", [[shoppingCenter Name] UTF8String], [[shoppingCenter getLevel] UTF8String]);
    
    Building *institute = [Institute new];
    institute.Name = @"Edificio 2";
    institute.Finished = [NSDate new];

    printf("El nivel de eficiencia del instituto %s es %s\n", [[institute Name] UTF8String], [[institute getLevel] UTF8String]);
  }
    return 0;
}

