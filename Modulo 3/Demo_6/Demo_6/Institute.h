//
//  Institute.h
//  Demo_5
//
//  Created by Mario Caballero Ramírez on 4/2/13.
//  Copyright (c) 2013 Curso iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "building.h"
#import "PowerEfficiency.h"

@interface Institute : Building<PowerEfficiency>

@end
