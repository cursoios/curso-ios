#import <Foundation/Foundation.h>
#import "building.h"

int main(int argc, const char * argv[])
{

  @autoreleasepool {
    Building *building1 = [Building new];
    building1.Name = @"Edificio 1";
    
    Building *building2 = [Building new];
    building2.Name = @"Edificio 2";

    NSDictionary *buildings = [[NSDictionary alloc] initWithObjectsAndKeys:building1,@"b1",building2,@"b2", nil ];

    for (NSString *buildingKey in buildings) {
      Building *building = [buildings objectForKey:buildingKey];
      printf("El edificio %s tiene un área de: %s\n", [[building Name] UTF8String], [[building getBuildingSquareMeters] UTF8String]);
    }
  }
    return 0;
}

