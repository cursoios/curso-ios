//
//  NSNumber+ElectricVehicle.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/19/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "NSNumber+ElectricVehicle.h"

@implementation NSNumber (ElectricVehicle)

- (NSString*) toPrintablePercentage : (int) withMaximumValue {
  int percentage = ([self intValue] * 100) / withMaximumValue;
  NSNumber *percentageNumber = [NSNumber numberWithInt:percentage];
  return [[percentageNumber stringValue] stringByAppendingString:@"%"];
}

@end
