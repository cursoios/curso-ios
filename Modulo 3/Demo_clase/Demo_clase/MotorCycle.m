//
//  MotorCycle.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/18/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "MotorCycle.h"

@implementation MotorCycle

- (id) init {
  self = [super initWithWheelsNumber:2];
  return self;
}

@end
