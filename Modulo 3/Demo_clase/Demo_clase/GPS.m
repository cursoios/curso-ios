//
//  GPS.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/20/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "GPS.h"

@implementation GPS

- (id) initWithModel : (NSString*) aModel
           andSeller : (NSString*) aSeller {
  
  if (self = [super init]) {
    self.Model = aModel;
    self.Seller = aSeller;
  }
  
  return self;
}

@end
