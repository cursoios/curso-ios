// ------------------------------------------------------------------------------------------------
// Vamos a crear un método nuevo dentro de NSNumber para poder mostrar la carga en porcentajes
// ------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "NSNumber+ElectricVehicle.h"

void categories() {

  printf("*********************************\nCategorías\n---------------------------------\n");

  NSNumber *number = @50;
  NSString *result = [number toPrintablePercentage:80];
  printf("Categorías: %s", [result UTF8String]);

  printf("\n\n");
}