//
//  main.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/17/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "1_initializers.m"
#import "2_hierarchy.m"
#import "3_collections.m"
#import "4_messages.m"
#import "5_protocols.m"
#import "6_categories.m"
#import "7_extensions.m"
#import "8_exceptions.m"

int main(int argc, const char * argv[])
{
  initializers();
  hierarchy();
  collections();
  messages();
  protocols();
  categories();
  extensions();
  exceptions();
  return 0;
}