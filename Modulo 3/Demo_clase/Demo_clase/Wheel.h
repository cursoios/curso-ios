//
//  Wheel.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/17/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Wheel : NSObject

@property (retain) NSNumber* Radius;

- (id) initWithRatio : (int)radius;

@end
