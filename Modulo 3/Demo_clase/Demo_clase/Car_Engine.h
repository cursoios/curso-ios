//
//  Car_Engine.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/19/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "Car.h"

@interface Car ()

- (NSNumber*) getCountValves;

@end
