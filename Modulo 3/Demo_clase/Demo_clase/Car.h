//
//  Car.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/18/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vehicle.h"

@interface Car : Vehicle

@property (retain) NSString* SeatsColor;

- (BOOL) start;

- (BOOL) stop;

- (void)setColor : (NSString*)aColor withSeatsColor:(NSString*)aSeatsColor;

@end
