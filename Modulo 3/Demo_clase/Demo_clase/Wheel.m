//
//  Wheel.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/17/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "Wheel.h"

@implementation Wheel

- (id) initWithRatio : (int)radius {
  if (self = [super init]) {
    self.Radius = [NSNumber numberWithInt:radius];
  }
  return self;
}

@end
