// ------------------------------------------------------------------------------------------------
// Vamos a crear un protocolo que permita definir que un coche es conducible por un humano
// ------------------------------------------------------------------------------------------------

#import "ElectricCar.h"

void protocols() {
  
  printf("*********************************\nProtocolos\n---------------------------------\n");
  
  ElectricCar *car = [ElectricCar new];
  printf("Porcentaje de batería: %d", [[car batteryPercentage] intValue]);

  printf("\n\n");
}