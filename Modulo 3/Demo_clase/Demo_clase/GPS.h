//
//  GPS.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/20/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GPS : NSObject

@property (retain) NSString* Model;
@property (retain) NSString* Seller;

- (id) initWithModel : (NSString*) aModel
           andSeller : (NSString*) aSeller;

@end
