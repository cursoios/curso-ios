// ------------------------------------------------------------------------------------------------
// Vamos a crear un método en la implementación de coche que permita conocer detalles del motor
// ------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "Car.h"
#import "Car_Engine.h"

void extensions() {
  printf("*********************************\nExtensiones\n---------------------------------\n");
  
  Car *car = [Car new];
  printf("Número de válvulas: %d", [[car getCountValves] intValue]);

  printf("\n\n");
}