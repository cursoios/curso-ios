// ------------------------------------------------------------------------------------------------
// Vamos a probar el paso de mensajes con métodos con diferente número de parámetros
// ------------------------------------------------------------------------------------------------

#import "Car.h"

void testMethods() {
  Car *car = [Car new];
  [car start];
  [car setColor:@"red"];
  [car setColor:@"red" withSeatsColor:@"blue"];
  
  unsigned long count = [car.Wheels count];
  printf("\nNúmero de ruedas: %lu", count);
  printf("\nNúmero de ruedas con string: %s", [[[NSNumber numberWithUnsignedLong:count] stringValue] UTF8String]);
}

void testSelectors() {
  Car *car = [Car new];
  SEL selector = @selector(getCountValves);
  
  printf("%d", [[car performSelector:selector] intValue]);
}

void messages() {
  printf("*********************************\nMensajes\n---------------------------------\n");
  
  //testMethods();
  testSelectors();

  printf("\n\n");
}