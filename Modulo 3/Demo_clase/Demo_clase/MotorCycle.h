//
//  MotorCycle.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/18/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vehicle.h"

@interface MotorCycle : Vehicle

@end
