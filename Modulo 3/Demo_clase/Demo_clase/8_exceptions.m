// ------------------------------------------------------------------------------------------------
// Vamos a ver como podemos gestionar las excepciones
// ------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "Vehicle.h"

// Se invoca al init y de vehicle que es clase abstracta
void initVehicle() {
  printf("\nProteger init sin parámetros:\n");
  
  Vehicle *vehicle;
  
  @try {
    vehicle = [[Vehicle alloc] init];
    for (Wheel *wheel in [vehicle Wheels])
      printf("Wheel: %s\n", [[wheel.Radius stringValue] UTF8String]);
  }
  @catch (NSException *exception) {
    printf("No se ha podido crear el objeto. Llamada a un método abstracto\n");
  }
  @finally {
    [vehicle release];
  }
}

void exceptions() {
  printf("*********************************\nExcepciones\n---------------------------------\n");
  
  initVehicle();
  
  printf("\n\n");
}