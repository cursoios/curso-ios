//
//  ElectricVehicle.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/19/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ElectricVehicle

- (NSNumber*) batteryPercentage;
- (NSNumber*) chargeStatus;
- (NSNumber*) charge;

@end
