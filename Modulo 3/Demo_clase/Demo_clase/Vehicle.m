//
//  Guitar.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/17/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "Vehicle.h"

@implementation Vehicle
@synthesize gps;

- (id) init {
  [NSException raise: @"init is an abstract method" format: @"init is an abstract method"];
  return nil;
}

- (id) initWithWheelsNumber : (int) numberOfWheels {
  if (self = [super init]) {
    self.Wheels = [NSMutableArray new];
    for (int i=0; i<numberOfWheels; i++)
      [self.Wheels addObject: [[Wheel alloc] initWithRatio:i]];
  }
  return self;
}

- (GPS*) gps {
  
  if (!self->gps)
    self->gps = [[GPS alloc] initWithModel:@"AK43" andSeller:@"Apple"];
  
  return self->gps;
}

- (void) setGps : (GPS*) aGps {
  self->gps = aGps;
}

+ (Vehicle*) vehicleWithWheelsNumber:(int)numberOfWheels {
  Vehicle *vehicle = [[Vehicle alloc] initWithWheelsNumber:numberOfWheels];
  return vehicle;
}

- (BOOL)start {
  [NSException raise: @"start is an abstract method" format: @"start is an abstract method"];
  return NO;
}

- (BOOL)stop {
  [NSException raise: @"stop is an abstract method" format: @"stop is an abstract method"];
  return NO;
}

// --------------------------------------------------------------------------------
// COMO SE IMPLEMENTAN LOS MODIFICADORES ASSIGN, RETAIN y COPY DE LAS PROPIEDADES -
// --------------------------------------------------------------------------------
/*
 
  // ASSIGN
  ----------
  - (void) setWheels : (NSMutableArray*) newWheels {
    self->_Wheels = newWheels;
  }

  // RETAIN
  ----------
  - (void) setWheels : (NSMutableArray*) newWheels {
    if (self->_Wheels != nil)
      [self->_Wheels release];

    self->_Wheels = newWheels;
    [self->_Wheels retain];
  }

  // COPY
  ----------
  - (void) setWheels : (NSMutableArray*) newWheels {
    if (self->_Wheels != nil)
      [self->_Wheels release];

    self->_Wheels = newWheels;
    [self->_Wheels copy];
  }
*/

@end