//
//  ElectricCar.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/19/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "ElectricCar.h"

@implementation ElectricCar

- (NSNumber*) batteryPercentage {
  return @100;
}

- (NSNumber*) chargeStatus {
  return @100;
}

- (NSNumber*) charge {
  return @100;
}

@end
