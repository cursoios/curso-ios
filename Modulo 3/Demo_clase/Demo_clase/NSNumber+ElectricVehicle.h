//
//  NSNumber+ElectricVehicle.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/19/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (ElectricVehicle)

- (NSString*) toPrintablePercentage : (int) withMaximumValue;

@end