// ------------------------------------------------------------------------------------------------
// Queremos un objeto que se cree forzozamente llamando a un contructor con un parámetro.
// Si se invocara la creación de un objeto sin parámetros, debería darnos una excepción
// ------------------------------------------------------------------------------------------------

#import "Vehicle.h"

void initVehicleWithWheels() {
  printf("\nLlamando a init con ruedas:\n");
  
  Vehicle *vehicle = [[Vehicle alloc] initWithWheelsNumber:4];
  for (Wheel *wheel in [vehicle Wheels])
    printf("wheel: %s\n", [[wheel.Radius stringValue] UTF8String]);

  [vehicle release];
}

void initVehicleWithStaticMethod() {
  printf("\nCrear vehículo desde método estático:\n");
  
  Vehicle *vehicle = [Vehicle vehicleWithWheelsNumber:4];
  for (Wheel *wheel in [vehicle Wheels])
    printf("wheel: %s\n", [[wheel.Radius stringValue] UTF8String]);
  
  [vehicle release];
}

void lazyLoading() {
  printf("\nCarga bajo demanda:\n");
  
  Vehicle *vehicle = [Vehicle vehicleWithWheelsNumber:4];
  printf("El gps es %s", [[[vehicle gps] Model] UTF8String]);
  
  [vehicle release];
}

void initializers() {
  printf("*********************************\nCreación de objetos\n---------------------------------\n");
    
  initVehicleWithWheels();
  initVehicleWithStaticMethod();
  lazyLoading();
  
  printf("\n\n");
}