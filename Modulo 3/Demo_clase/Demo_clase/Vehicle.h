//
//  Guitar.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/17/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Wheel.h"
#import "GPS.h"

@interface Vehicle : NSObject

@property (retain) NSMutableArray* Wheels;
@property (retain) NSNumber* doorsCount;
@property (retain) NSString* Color;
@property (retain) GPS* gps;

- (id) init;

- (id) initWithWheelsNumber : (int)numberOfWheels;

+ (Vehicle*) vehicleWithWheelsNumber : (int)numberOfWheels;

- (BOOL)start;

- (BOOL)stop;

@end
