// ------------------------------------------------------------------------------------------------
// Vamos a jugar con los tipos de colecciones que ofrece Objective-C
// ------------------------------------------------------------------------------------------------

#import "Car.h"
#import "MotorCycle.h"

void createArray() {
  printf("\n--> Arrays:");
  
  NSArray* carArray = [NSArray arrayWithObjects:[MotorCycle new], [Car new], nil];
  //NSArray* inlineCarArray = @[[Car new], [MotorCycle new]];
  
  for (Vehicle *vehicle in carArray) {
    const char* className = [NSStringFromClass([vehicle class]) UTF8String];
    printf("\nVehículo de tipo %s de %lu ruedas", className, [[vehicle Wheels] count]);
  }
  
  // Método que diferencia NSArray de NSSet
  printf("\nElemento de la posición 0 de tipo: %s", [NSStringFromClass([[carArray objectAtIndex:0] class]) UTF8String]);
}

void createMutableArray() {
  printf("\n\n--> Mutable Arrays:");

  NSMutableArray* carArray = [NSMutableArray new];
  [carArray addObject:[Car new]];
  [carArray addObject:[MotorCycle new]];
  [carArray addObject:[Car new]];
  
  for (Vehicle *vehicle in carArray) {
    const char* className = [NSStringFromClass([vehicle class]) UTF8String];
    printf("\nVehículo de tipo %s de %lu ruedas", className, [[vehicle Wheels] count]);
  }
}

void createSet() {
  printf("\n\n--> Listas:");
  
  NSSet* carSet = [NSSet setWithObjects:[MotorCycle new], [Car new], nil];
  for (Vehicle *vehicle in carSet) {
    const char* className = [NSStringFromClass([vehicle class]) UTF8String];
    printf("\nVehículo de tipo %s de %lu ruedas", className, [[vehicle Wheels] count]);
  }
}

void createMutableSet() {
  printf("\n\n--> Mutable Listas:");
  
  NSMutableSet* carSet = [NSMutableSet new];
  [carSet addObject:[Car new]];
  [carSet addObject:[MotorCycle new]];
  [carSet addObject:[Car new]];
  
  for (Vehicle *vehicle in carSet) {
    const char* className = [NSStringFromClass([vehicle class]) UTF8String];
    printf("\nVehículo de tipo %s de %lu ruedas", className, [[vehicle Wheels] count]);
  }
}

void createDictionary() {
  printf("\n\n--> Diccionarios:");
  
  NSDictionary* carDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[Car new], @"car1", nil];
  for (NSString *vehicleKey in carDictionary) {
    Vehicle *vehicle = [carDictionary valueForKey:vehicleKey];
    const char* className = [NSStringFromClass([vehicle class]) UTF8String];
    printf("\nVehículo de tipo %s y nombre %s de %lu ruedas", className, [vehicleKey UTF8String], [[vehicle Wheels] count]);
  }
}

void createMutableDictionary() {
  printf("\n\n--> Mutable Diccionarios:");
  
  NSMutableDictionary* carDictionary = [NSMutableDictionary new];
  [carDictionary setObject:[Car new] forKey:@"car1"];
  for (NSString *vehicleKey in carDictionary) {
    Vehicle *vehicle = [carDictionary valueForKey:vehicleKey];
    const char* className = [NSStringFromClass([vehicle class]) UTF8String];
    printf("\nVehículo de tipo %s y nombre %s de %lu ruedas", className, [vehicleKey UTF8String], [[vehicle Wheels] count]);
  }
}

void collections() {
  
  printf("*********************************\nColecciones\n---------------------------------\n");

  createArray();
  createMutableArray();
  createSet();
  createMutableSet();
  createDictionary();
  createMutableDictionary();
  
  printf("\n\n");
}