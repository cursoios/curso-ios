// ------------------------------------------------------------------------------------------------
// Vamos a probar la creación de vehiculos y lo que queremos es invocar a un método abstracto
// que se llame arrancar
// ------------------------------------------------------------------------------------------------

#import "Car.h"

void hierarchy() {
  printf("*********************************\nJerarquía\n---------------------------------\n");
  
  Car *car = [Car new];
  for (Wheel *wheel in [car Wheels])
    printf("\nwheel: %s", [[wheel.Radius stringValue] UTF8String]);
  

  if ([car start])
    printf("\nEl coche ha arrancado");
  
  printf("\n\n");
}