//
//  ElectricCar.h
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/19/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "Car.h"
#import "ElectricVehicle.h"

@interface ElectricCar : Car<ElectricVehicle>
@end