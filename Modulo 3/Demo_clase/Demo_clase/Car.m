//
//  Car.m
//  Objetos
//
//  Created by Mario Caballero Ramírez on 3/18/13.
//  Copyright (c) 2013 Mario Caballero Ramírez. All rights reserved.
//

#import "Car.h"
#import "Car_Engine.h"

@implementation Car

- (id) init {
  self = [super initWithWheelsNumber:4];
  return self;
}

- (BOOL)start {
  return true;
}

- (BOOL)stop {
  return true;
}

- (void)setColor : (NSString*)aColor withSeatsColor:(NSString*)aSeatsColor {
  self.Color = aColor;
  self.SeatsColor = aSeatsColor;
}

- (NSNumber*) getCountValves {
  return @16;
}

@end
